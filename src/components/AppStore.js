import Reflux from "reflux";
import update from "immutability-helper";
import AppActions from "./AppActions";

const _getInitialData = () => {
  return {
    inputValue: "",
    messageValue: "Reflux example!"
  };
};

class AppStore extends Reflux.Store {
  constructor() {
    super();
    this.listenables = AppActions;
    this.state = _getInitialData();
  }

  onChangeInput(value) {
    this.setState(update(this.state, { inputValue: { $set: value } }));
  }

  onChangeMessage(value) {
    this.setState(update(this.state, { messageValue: { $set: value } }));
  }
}

export default AppStore;
