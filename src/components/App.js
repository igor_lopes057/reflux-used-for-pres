import React from "react";
import Reflux from "reflux";
import AppActions from "./AppActions";
import AppStore from "./AppStore";
import "../App.css";

class App extends Reflux.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.store = AppStore;
  }

  _changeInputValue = event => {
    const { value } = event.target;
    AppActions.ChangeInput(value);
  };

  _updateMessageOnScreen() {
    const { inputValue } = this.state;
    AppActions.ChangeMessage(inputValue);
  }

  render() {
    const { inputValue, messageValue } = this.state;
    const isEasterEgg = messageValue === 'plus ultra!';
    return (
      <div className="App" style={{ paddingTop: "10px" }}>
        <input
          type="text"
          onChange={this._changeInputValue}
          value={inputValue}
          placeholder={"Say something..."}
          autoFocus
        />
        <button onClick={() => this._updateMessageOnScreen()}>Click me!</button>
        <h1>{!isEasterEgg ? messageValue : "VÁ ALÉM!!!"}</h1>
        {isEasterEgg && (
          <iframe
            title="PLUS ULTRA"
            src="https://giphy.com/embed/YhiHhcvYOROes"
            width="480"
            height="269"
            frameBorder="0"
            class="giphy-embed"
          />
        )}
      </div>
    );
  }
}

export default App;
