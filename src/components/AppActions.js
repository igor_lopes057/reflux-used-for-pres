import Reflux from "reflux";

const AppActions = Reflux.createActions(["ChangeInput", "ChangeMessage"]);

export default AppActions;
